//
//  DrawingView.swift
//  AppPencil
//
//  Created by Panupong Chaiyarut on 22/2/2564 BE.
//

import UIKit
import PencilKit

class DrawingView: PKCanvasView {
    
    private var lastPoint: CGPoint = .zero
    private var currentPath = UIBezierPath()
    private var currentLayer: CAShapeLayer = CAShapeLayer()
    private var pencil = Pencil()
    //let canvasView = UIView()
    let mainImageView = PKCanvasView()
    var isDrawing: Bool = true
    
    internal var underlayView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var image: UIImage = {
        return #imageLiteral(resourceName: "fish")
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
//        [mainImageView,canvasView].forEach {
//            addSubview($0)
//            // $0.pinToSuperViewEdges()
//        }
        
        addSubview(underlayView)
        NSLayoutConstraint.activate([
            underlayView.topAnchor.constraint(equalTo: topAnchor),
            underlayView.leftAnchor.constraint(equalTo: leftAnchor),
            underlayView.rightAnchor.constraint(equalTo: rightAnchor),
            underlayView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint,color: UIColor,size: CGSize) {
        
        let currentLayer = CAShapeLayer()
        let currentPath = UIBezierPath()
        
        currentPath.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        currentPath.addLine(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        currentPath.addLine(to: CGPoint(x: fromPoint.x+size.width, y: toPoint.y))
        currentPath.addLine(to: CGPoint(x: fromPoint.x+size.width, y: toPoint.y+size.height))
        currentPath.addLine(to: CGPoint(x: fromPoint.x, y: toPoint.y+size.height))
        //currentPath.stroke()
        
        currentLayer.fillColor = color.cgColor
        currentLayer.path = currentPath.cgPath
        currentLayer.lineWidth = 1//pencil.strokeSize
        //currentLayer.lineCap = .round
        //currentLayer.lineJoin = .round
        //currentLayer.strokeColor = pencil.color.cgColor
        
        underlayView.layer.addSublayer(currentLayer)
        underlayView.setNeedsDisplay()
    }
    
    func clear(){
        print(" clear Line ")
        currentPath.removeAllPoints()
        underlayView.layer.sublayers = nil
        underlayView.setNeedsDisplay()
        //underlayView.setNeedsLayout()
    }

    
  
}

struct Pencil {
    let color: UIColor = .red
    let strokeSize: CGFloat = 8
    let outlineSize: CGFloat = 12
}

struct model {
    var x: Double!
    var y: Double!
    var color: UIColor!
    
    init(x: Double,y: Double,color: UIColor) {
        self.x = x
        self.y = y
        self.color = color
    }
}
