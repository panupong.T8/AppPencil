//
//  MainViewController.swift
//  AppPencil
//
//  Created by Nuttapon.T8 on 9/2/2564 BE.
//

import UIKit
import PencilKit

class MainViewController: UIViewController {
    
    let canvasView = DrawingView()
    var x: CGFloat = 0
    var n: Int = 0
    var y: CGFloat = 0
    var timer: Timer?
    var size: Int = 5
    var plotmodel: [model] = [model(x: 0, y: 0,color: .blue)]
  
    
    
    
    let imageName = "fish"
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setNavigationBar()
        view.backgroundColor = .white
        canvasView.contentInsetAdjustmentBehavior = .never
        canvasView.delegate = self
        canvasView.isOpaque = false
        canvasView.maximumZoomScale = 3.0
        canvasView.contentOffset = CGPoint.zero
        canvasView.backgroundColor = .clear
        // MARK: fixSize
        //canvas.contentSize = canvas.canvasView.frame.size
        canvasView.contentSize = CGSize(width: 1600, height: 1071) ///////////////
        canvasView.frame = CGRect(x: 0.0, y: 50.0, width: 768.0, height: 974.0)
        
        canvasView.underlayView.contentMode = .scaleToFill
        canvasView.underlayView.layer.borderColor = UIColor.orange.cgColor
        canvasView.underlayView.layer.borderWidth = 1.0
        canvasView.underlayView.backgroundColor = UIColor(patternImage: UIImage(named:imageName)!)
        canvasView.underlayView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 1600, height: 1071))
        // MARK: fixSize
        print(" canvas => ",canvasView.frame,"\n canvas.contentsize => ",canvasView.contentSize,"\n canvasView => ",canvasView.underlayView.frame)
        //StartTimer()
        //drawHeart()
        filter()
        let msg = greetUser(name: "Jack", age: -2)
        print(msg.name)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard
            let window = view.window,
            let toolPicker = PKToolPicker.shared(for: window) else { return }
        
        toolPicker.setVisible(true, forFirstResponder: canvasView)
        toolPicker.addObserver(canvasView)
        canvasView.becomeFirstResponder()
        
        //        if let window = UIApplication.shared.windows.first {
        //            if let toolPicker = PKToolPicker.shared(for: window) {
        //                toolPicker.addObserver(canvasView)
        //                toolPicker.setVisible(true, forFirstResponder: canvasView)
        //                canvasView.becomeFirstResponder()
        //            }
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.canvasView.sendSubviewToBack(canvasView.underlayView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // MARK: fixSize
        //let contentSize = canvas.canvasView.frame.size
        let contentSize = CGSize(width: 1600, height: 1071)
        canvasView.contentSize = contentSize
        canvasView.underlayView.frame = CGRect(origin: CGPoint.zero, size: contentSize)
        // MARK: fixSize
        //let margin = (canvas.bounds.size - contentSize) * 0.5
        let margin = CGSize(width: -416, height: -48.5)
        let insets = [margin.width, margin.height].map { $0 > 0 ? $0 : 0 }
        canvasView.contentInset = UIEdgeInsets(top: insets[1], left: insets[0], bottom: insets[1], right: insets[0])
        print(" canvas => ",canvasView.frame,"\n canvas.contentsize => ",canvasView.contentSize,"\n canvasView => ",canvasView.underlayView.frame," \ncanvas.contentInset => ",canvasView.contentInset," \nmargin ",margin )
    }
    
    //    @objc func drawLine() {
    //        if Int(plotmodel[0].x)+(5*n) >= Int(view.frame.size.width) {
    //            n = 0
    //            y += 5
    //            self.canvas.drawLine(from: CGPoint(x: Int(plotmodel[0].x), y: y), to: CGPoint(x: Int(plotmodel[0].x), y: y), color: randomColor(), size: CGSize(width: 5, height: 5))
    //        } else if y >= Int(view.safeAreaLayoutGuide.layoutFrame.height) {
    //            timer?.invalidate()
    //        }
    //        else{
    //            self.canvas.drawLine(from: CGPoint(x: Int(plotmodel[0].x)+(5*n), y: y), to: CGPoint(x: Int(plotmodel[0].x)+(5*n), y: y), color: randomColor(), size: CGSize(width: 5, height: 5))
    //        }
    //        n += 1
    //    }
    
    @objc func randomPoint() {
        let width = Int.random(in: 0...Int(view.frame.size.width))
        let height = Int.random(in: 0...Int(view.safeAreaLayoutGuide.layoutFrame.height))
        self.canvasView.drawLine(from: CGPoint(x: width, y: height), to: CGPoint(x: width, y: height), color: randomColor(), size: CGSize(width: 5, height: 5))
    }
    
    func randomColor() -> UIColor{
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    func setNavigationBar() {
        let toggleItem = UIBarButtonItem(title: "Toggle Picker", style: .plain, target: self, action: #selector(togglePicker))
        navigationItem.rightBarButtonItem  = toggleItem
    }
    
    @objc func togglePicker() {
        if canvasView.isFirstResponder{
            canvasView.resignFirstResponder()
            StartTimer()
        }else{
            canvasView.becomeFirstResponder()
            timer?.invalidate()
        }
    }
    
    func StartTimer() {
        timer?.invalidate()
        y = canvasView.bounds.minY
        //run()
        canvasView.clear()
        timer = Timer.scheduledTimer(timeInterval: -99999, target: self, selector: #selector(run), userInfo: nil, repeats: true)
    }
    
    func setView(){
        view.addSubview(canvasView)
        NSLayoutConstraint.activate([
            canvasView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            canvasView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            canvasView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            canvasView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    let prices = [99,199,299]
    let optionalNumbers = [1, 2, nil, 4, nil]
    
    let arrayLength = 10_000_000

    var numbers = [Int]()
 
    let range = ..<2
    
    let someString = "Hello, World!"
    
    enum Personnel {
       case intern(name: String, advisor: String)
       case contentWriter(name: String, contentType: String)
       case manager(name: String, departmentNo: Int)
    }
    
    let password = "myPass1234"
    
    enum stay{
        case go
        case left
        case right
    }
    
    var str : String?

    
    
    func filter(){
        
//        for index in 1...arrayLength {
//            numbers.append(index)
//        }
        
        print("filter ==> ","\(prices.filter { $0 > 99 })") //หาเงื่อนไข
        
        print("flatMap =>> \(optionalNumbers.flatMap { $0 })") //ไม่เอา nil
        
        print("map ==> ","\(optionalNumbers.map { $0 })") // มีค่า nil
        
//        print(" test Map ==> \(numbers.filter { $0 % 1000000 == 0  })")
//
//        print(" test Map ==> \(numbers.filter { $0 >= 9900000  })")
//
//        for num in numbers where num >= 9900000 {
//            print(" for loop ",num)
//        }
//
        myFunc(name:"Milo", "I'm a really old wizard")
        
        print("range ",range.contains(3))
    
        let person = Personnel.contentWriter(name: "Guitar", contentType: "PK")
        
        switch person {
        case .intern(let name, let advisor):
            print("The intern name is " + name + " has advisor name, " + advisor)
        case .contentWriter(let name, var contentType):
            print("The content writer name " + name + " is writing content " + contentType)
        case .manager(let name, let departmentNo):
            print("The manager of department number \(departmentNo) is name " + name)
        }
        let isPasswordLongerThan6Characters = password.count > 6
        
        guard isPasswordLongerThan6Characters else {
            print("ไม่ผ่าน")
            return
        }
        print("ผ่าน")
        
        
//        var myway = stay.go
//        myway = stay.left
        
        switchcase(myway: stay.go)
        switchcase(myway: stay.left)
        

    }
    
    func switchcase(myway: stay) {
        
        switch myway {
        case .go:
            print("let's Go")
        case .left:
            print("let's Go to the left")
        case .right:
            print("let's Go to the right")
        default:
            print("stay Here")
        }
    }
    
  
    
    func myFunc(name:String, _ age:String){
        print("My Function\n my name is ",name," I'm ",age," years old")
    }
    
    func greetUser(name:String, age:Int) -> (name:String,age: Int) {
        
        let msg = "Good Morning!" + name
        var userage = age
        if age < 0 {
                userage = 0
        }
        return (msg, userage)
    }
   
    
    
}

extension MainViewController: PKCanvasViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        canvasView.underlayView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        switch scrollView {
        case canvasView:
            // print(Self.self, #function)
            let offsetX: CGFloat = max((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0)
            let offsetY: CGFloat = max((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0)
            print(" offsetX=> ",offsetX)
            print(" offsetY ",offsetY)
            canvasView.underlayView.frame.size = CGSize(width: 1600.0, height: 1071.0) * self.canvasView.zoomScale//canvas.frame.size * self.canvas.zoomScale
            canvasView.underlayView.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
            
            print(" underlayView.center => ",canvasView.underlayView.center)
            
        default:
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case canvasView:
            //print(Self.self, #function)
            StartTimer()
        default:
            break
        }
    }
    
    @objc func run(){
        if canvasView.bounds.minX+CGFloat((size*n)) > canvasView.bounds.maxX {
            n = 0
            y += 5
            self.canvasView.drawLine(from: CGPoint(x: Int(canvasView.bounds.minXminY.x)+(size*n), y: Int(y)), to: CGPoint(x: Int(canvasView.bounds.minXminY.x)+(size*n), y: Int(y)), color: randomColor(), size: CGSize(width: size, height: size))
        }
        //        else if y >= Int(view.safeAreaLayoutGuide.layoutFrame.height) {
        //            timer?.invalidate()
        //        }
        else{
            canvasView.drawLine(from: CGPoint(x: Int(canvasView.bounds.minXminY.x)+(size*n), y: Int(y)), to: CGPoint(x: Int(canvasView.bounds.minXminY.x)+(size*n), y: Int(y)), color: randomColor(), size: CGSize(width: size, height: size))
        }
        n += 1
    }
    
    
    
    @objc func drawHeart(){
        for i in n...5{
            canvasView.drawLine(from: CGPoint(x: Int(view.center.x)+(i*size), y: Int(view.center.y)),to: CGPoint(x: Int(view.center.x)+(i*size), y: Int(view.center.y)),color: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),size: CGSize(width: size, height: size))
            canvasView.drawLine(from: CGPoint(x: Int(view.center.x)-(i*size), y: Int(view.center.y)),to: CGPoint(x: Int(view.center.x)-(i*size), y: Int(view.center.y)),color: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),size: CGSize(width: size, height: size))
            for j in 0...(5-i) {
                canvasView.drawLine(from: CGPoint(x: Int(view.center.x)+(i*size), y: Int(view.center.y)+(j*size)),to: CGPoint(x: Int(view.center.x)+(i*size), y: Int(view.center.y)+(j*size)),color: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),size: CGSize(width: size, height: size))
                canvasView.drawLine(from: CGPoint(x: Int(view.center.x)-(i*size), y: Int(view.center.y)-(j*size)),to: CGPoint(x: Int(view.center.x)+(i*size), y: Int(view.center.y)+(j*size)),color: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),size: CGSize(width: size, height: size))
            }
             
        }
        
    }
    
    
}

import SwiftUI
@available(iOS 13.0, *)
struct HomePreview: PreviewProvider {
    
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }

    struct ContainerView: UIViewControllerRepresentable {
        func makeUIViewController(context: UIViewControllerRepresentableContext<HomePreview.ContainerView>) -> UIViewController {
            return UINavigationController(rootViewController: MainViewController())
        }

        func updateUIViewController(_ uiViewController: HomePreview.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<HomePreview.ContainerView>) {
        }
    }
    
}
